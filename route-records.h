#include <stdio.h>
// [2 point] - header guard
#ifndef ROUTE_RECORDS_H
#define ROUTE_RECORDS_H

// [2 point] - enum and struct
typedef struct RouteRecord {
    char origin_IATA[3+1];
    char dest_IATA[3+1];
    char airline[2+1];
    int total_passengers[6];
} RouteRecord;

typedef enum SearchType {
    ROUTE,
    ORIGIN,
    DESTINATION,
    AIRLINE,
} SearchType;

// [2 point] - function prototypes
RouteRecord* createRecords(FILE* in);

int fillRecords(RouteRecord* r, FILE* in);

int findAirlineRoute(RouteRecord* r, int length, const char* origin, 
    const char* destination, const char* airline, int curIdx);
    
void searchRecords( RouteRecord* r, int length, const char* key1, const char* key2, SearchType st );

void printMenu();
#endif