#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "route-records.h"

int main( int argc, char *argv[] )
{
    /* 2. Check command line arguments here. If a command line argument (for the file name) is missing, print out the following: ERROR: Missing file name and end the program */
    // [1 point] - Processes command-line argument for input file name
    if (argc < 2) {
        printf("ERROR: missing file\n");
        return -1;
    }
    /* 1. Declare variables here */
    

    /* 3. Attempt to open the file. Print out Opening <filename>... before you call fopen(). */
    /* 4. Check to see if the file opens. If it does not open, print out ERROR: Could not open file and end the program. */
    
    printf("Opening %s ...\n", argv[1]);
    // [1 point] - Open files and checks for success in opening
    FILE* in = fopen(argv[1], "r");
    if (in == NULL) {
        printf("ERROR: Could not open file");
        return -1;
    }
    // [2 points] - Sets up the array of RouteRecords
    RouteRecord* records = createRecords(in);
    int length = fillRecords(records, in);
    fclose(in);
    /* 5. Do the following to load the records into an array of RouteRecords
    	
    		5.1 Call createRecords(), which will read through the file once to find the total number of lines/records in the file. Use this count, to dynamically allocate memory for an array of RouteRecords. It returns a pointer to that array. Don't forget to rewind the file pointer.
    		
    		5.2 Call fillRecords() to go through the CSV file again to fill in the values. It will then return the actual number of items the array has. Recall that not all records in the original CSV file will be entered into the array. Print the number of unique routes operated by different airlines: Unique routes operated by airlines: ???
    		
    		5.3 Close the the file.
    */
    // 6. Create an infinite loop that will do the following:
   
    while (1) {
        // 6.1 Call printMenu() *
        int option = 0;
        printMenu();
        // 6.2 Ask the user to input a value for the menu *
        // [2 points] - Processes user input, handling bad input
        if (!scanf("%d", &option)) {
            option = 6;
            getchar();
        }
        printf("\n");

        // 6.4 Create a switch/case statement to handle all the menu options *
        switch(option) {
            case 1: { // by route
                char ori[4];
                char dest[4];
                // 6.4.1 Each option requires the user to enter a search key *
                // [2 points] - asks user for search keys and calls appropriate functions for
                printf("Enter Origin: ");
                scanf("%3s", ori);
                printf("Enter Destination: ");
                scanf("%3s", dest);
                printf("Searching by route...\n");
                searchRecords(records, length, ori, dest, ROUTE);
                break;
            }
            case 2: { // by origin
                char ori[4];
                printf("Enter Origin: ");
                scanf("%3s", ori);
                printf("Searching by origin...\n");
                searchRecords(records, length, ori, NULL, ORIGIN);
                break;
            }
            case 3: { // by dest
                char dest[4];
                printf("Enter Destination: ");
                scanf("%3s", dest);
                printf("Searching by destination...\n");
                searchRecords(records, length, dest, NULL, DESTINATION);
                break;
            }
            case 4: { // by airline
                char airline[3];
                printf("Enter Airline: ");
                scanf("%2s", airline);
                printf("Searching by airline...\n");
                searchRecords(records, length, airline, NULL, AIRLINE);
                break;
            }
            case 5: { // quit
                // 6.4.2 Quit needs to free the array *
                free(records);
                // int* f = NULL;
                // (*f) = 5; // ^_^
                printf("Good-bye!\n");
                exit(0);
            }
            default:
                // 6.3 Handle the case in which a non-integer value is entered *
                printf("Invalid choice.\n");
                break;
        }
        //getchar(); // Leftover \n
    }

    return 0;
}
