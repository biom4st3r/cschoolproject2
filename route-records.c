#include "route-records.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

void printMenu() {
    printf( "\n\n######### Airline Route Records Database MENU #########\n" );
    printf( "1. Search by Route\n" );
    printf( "2. Search by Origin Airport\n" );
    printf( "3. Search by Destination Airport\n" );
    printf( "4. Search by Airline\n" );
    printf( "5. Quit\n" );
    printf( "Enter your selection: " );
}

void _init_passengers(RouteRecord* route) {
    for(int k = 0; k < 6; k++) {
        route->total_passengers[k] = 0;
    }
}

RouteRecord* createRecords(FILE* in) {
    // Go through csv and count records
    char line[50];
    int i;
    // [1 point] - Traverses the CSV file and counts the number of lines, excluding header row
    for (i = 0; fgets(line, 50, in) != NULL; i++) {}
    // [1 point] - Dynamically allocates memory for the array of RouteRecords based on the number of lines counted
    RouteRecord* records = (RouteRecord*)malloc(sizeof(RouteRecord) * (i-1)); // Minus 1 for header
    // init all passengers to 0
    for (int j = 0; j < (i-1); j++) {
        _init_passengers(records + j); // [1 point] - Initializes the passenger data array within each RouteRecord struct object
    }
    rewind(in); // Rewinds the file pointer
    return records; // [1 point] - Returns the pointer to the array
}

int fillRecords(RouteRecord* r, FILE* in) {
    int month = 0;
    int count = 0;
    RouteRecord t;
    int i = 0;
    // [1 point] - Traverses the CSV file skipping the header row
    while (fgetc(in) != '\n'); // skip header
    while(fscanf(in, "%d,%3s,%3s,%2s,%*10s,%d", &month, t.origin_IATA, t.dest_IATA, t.airline, &count) == 5) {
        // [1 point] - Calls findAirlineRoute() to see if route operated by airline already exists in the array
        int idx = findAirlineRoute(r, i, t.origin_IATA, t.dest_IATA, t.airline, 0);
        if (!(idx + 1)) { // -1 + 1 = 0 AKA not found // [1 point] - If not found, adds the new route operated by an airline into the array
            _init_passengers(&t);
            idx = i; // use idx as SOT
            r[idx] = t;
            i++; // increment size of array
        }
        // All cases need the month set
        r[idx].total_passengers[month-1] = count; // [1 point] - If found, updates an existing route operated by an airline in the array with the new month's passenger data 
        
    }
    rewind(in);
    return i; // [2 points] - Returns the unique number of routes as operated by different airlines. This will be the number of items in the array that are actually used. 
}

int findAirlineRoute(RouteRecord* r, int length, const char* origin, 
        const char* destination, const char* airline, int curIdx) {
    
    if (curIdx+1 >= length) { // TODO - this might not be correct
        return -1; // [1 point] - Returns -1 if not found
    } else if (strcmp(r[curIdx].dest_IATA, destination) == 0 && strcmp(r[curIdx].origin_IATA, origin) == 0 && strcmp(r[curIdx].airline, airline) == 0) {
        return curIdx; // [1 point] - Returns the index of the array in which it was found
    }
    // why recursive? recursion makes me sad T_T
    return findAirlineRoute(r, length, origin, destination, airline, curIdx+1); // [6 points] - RECURSIVE function set up to find the airline route by checking if there a struct object that matches the origin, destination, and airline
}

// print out the airline and route for each match
// print total number of passenger on all matches
// total number of passengers by month
// average numbers of passengers per month
void _increment_total(int* passengers, RouteRecord* route) {
    for (int i = 0; i < 6; i++) {
        passengers[i] = passengers[i] + route->total_passengers[i];
    }
}

void searchRecords( RouteRecord* r, int length, const char* key1, const char* key2, SearchType st ) {
    int passengers[6] = {0,0,0,0,0,0};
    int hits = 0;
    for (int i = 0; i < length; i++) { // [1 point] - Traverses the array
        RouteRecord* route = r+i;
        int match = 0;
        switch(st) { // [1 point] - Determines the search type and compares key(s) against appropriate data members
            case AIRLINE:
                match = strcmp(route->airline, key1) == 0;
                break;
            case DESTINATION:
                match = strcmp(route->dest_IATA, key1) == 0;
                break;
            case ORIGIN:
                match = strcmp(route->origin_IATA, key1) == 0;
                break;
            case ROUTE:
                match = strcmp(route->origin_IATA, key1) == 0 && strcmp(route->dest_IATA, key2) == 0;
                break;
        }
        if (match) {
            hits++;
            _increment_total(passengers, route);
            printf("%s (%s-%s) ", route->airline, route->origin_IATA, route->dest_IATA); // [2 points] - Prints all matching information AirlineCode (Origin-Destination)
        }
    }
    // [4 points] - Calculates and prints out total passengers, total passengers per month, and average
    printf("\n%d matches were found\n\n", hits);
    printf("Statistics\n");
    int total = passengers[0] + passengers[1] + passengers[2] + passengers[3] + passengers[4] + passengers[5];
    printf("Total Passengers: %23d\n", total);
    for (int k = 0; k < 6; k++) {
        printf("Total Passengers in Month %d: %12d\n", k + 1, passengers[k]);
    }
    printf("\nAverage Passengers per Month: %11d\n", total / 6);
}